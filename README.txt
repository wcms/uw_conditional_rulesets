Requirements
------------
Drupal 7.x

Installation
------------
Upon installation, the following are required:
1) Webform module installed
2) Tokens module installed

Description
-----------
This module is developed to extend the functionality of the Webform module.

The module can be split into two parts:
1) Ruleset Categories
2) Confirmation page templating

A new Webform component will be available upon installation. The new component will
be named Ruleset Category. A ruleset is a set which contains a conditional and three fields of text:
a result, a description, and a URL. A ruleset category is a bucket that will hold
rulesets. Under Webform's conditionals page, a ruleset category can use the 'containing'
action in order to add a ruleset to a ruleset category if the conditional holds true.
Each ruleset category is required to have a default ruleset such that if no conditionals
evaluate to true, then a default can be used. In addition to the default ruleset,
a value for the number of results needs to be specified. This value indicates how many
results will need to be pulled from the ruleset category. If the number of results
needed to be pulled is less than the number of conditionals rulesets that evaluate to
true then the default ruleset will be used for the rest. Once a ruleset category
is saved, tokens will be generated. The number of tokens that get generated will
depend on the number of results specified in the ruleset category. Each token name
is dependent on the form key.

Generally, a conditional ruleset token will have the form [conditional_rulesets:form_key_part#_ nid]
form_key should be replaced with the actual form key of the ruleset category.
part should be replaced with either result, description, or url.
# should be replaced with a number from 1 to number of results specified in the ruleset category.
nid should be replaced by the node ID of the webform.

In order to use the conditional rulesets on a page, simply insert an appropriate token
in a token-enabled textarea.

Linking a template to a Webform is done through the configuration page. Under
Dashboard -> Conditional Rulesets Form Settings, create a new configuration.
A configuration will consist of 3 fields: A node ID, a template name, and form IDs.
The node ID is the Webform's node ID. The template name is the name of the template
used for theming the Webform's confirmation page. The form IDs are optional and is
used if the template itself supplies a FillPDF link and needs the IDs to generate the link.

To add a new template to the module, developers must inplement
hook_conditional_rulesets_preprocess(). The hook takes has 2 arguments: &$theme_registry
and $query. &$theme_registry is a reference of the data for theming while
$query is a query from the conditional_rulesets_config table. Once a module has
implemented the hook, any templates residing in the templates folder will be added
to the list of options when choosing a template name.