<?php

/**
 * Created by PhpStorm.
 * User: evnguyen
 * Date: 1/16/2018
 * Time: 9:28 AM
 * @file
 * Child class of WebformConditionals. Only check for ruleset cateogry webform
 * components to apply the action.
 */

/**
 * Inherits from WebformConditionals.
 *
 * Class RulesetConditionals
 */
class RulesetConditionals extends WebformConditionals {
  protected static $conditionals = array();

  /**
   * Creates and caches a RulesetConditional for a given node.
   *
   * @param $node
   *
   * @return mixed
   */
  static function factory($node) {
    if (!isset(self::$conditionals[$node->nid])) {
      self::$conditionals[$node->nid] = new RulesetConditionals($node);
      //parent::$conditionals[$node->nid] = self::$conditionals[$node->nid];
    }
    return self::$conditionals[$node->nid];
  }

  /**
   * RulesetConditionals constructor.
   *
   * @param $node
   */
  function __construct($node) {
    parent::__construct($node);
  }

  /**
   * Executes the conditionals on a submission, removing any data which should
   * be hidden.
   * This function imitates very closely to WebfromConditionals::executeConditionals
   * in webform.webformconditionals.inc, the only difference is that it only checks for
   * the 'add_to' action type.
   */
  function executeRulesetConditionals($input_values, $page_num = 0) {
    $this->getOrder();
    $this->getChildrenMap();

    $this->requiredMap[$page_num] = array();
    $this->setMap[$page_num] = array();
    $this->markupMap[$page_num] = array();

    module_load_include('inc', 'webform', 'includes/webform.conditionals');

    $components = $this->node->webform['components'];
    $conditionals = $this->node->webform['conditionals'];
    $operators = webform_conditional_operators();
    $targetLocked = array();

    $first_page = $page_num ? $page_num : 1;
    $last_page = $page_num ? $page_num : count($this->topologicalOrder);
    for ($page = $first_page; $page <= $last_page; $page++) {
      foreach ($this->topologicalOrder[$page] as $conditional_spec) {

        $conditional = $conditionals[$conditional_spec['rgid']];
        $source_page_nums = array();

        // Execute each comparison callback.
        $this->executionStackInitialize($conditional['andor']);
        foreach ($conditional['rules'] as $rule) {
          switch ($rule['source_type']) {
            case 'component':
              $source_component = $components[$rule['source']];
              $source_cid = $source_component['cid'];

              $source_values = array();
              if (isset($input_values[$source_cid])) {
                $component_value = $input_values[$source_cid];
                // For select_or_other components, use only the select values because $source_values must not be a nested array.
                // During preview, the array is already flattened.
                if ($source_component['type'] === 'select' &&
                  !empty($source_component['extra']['other_option']) &&
                  isset($component_value['select'])) {
                  $component_value = $component_value['select'];
                }
                $source_values = is_array($component_value) ? $component_value : array($component_value);
              }

              // Determine the operator and callback.
              $conditional_type = webform_component_property($source_component['type'], 'conditional_type');
              $operator_info = $operators[$conditional_type];

              // Perform the comparison callback and build the results for this group.
              $comparison_callback = $operator_info[$rule['operator']]['comparison callback'];
              // Contrib caching, such as entitycache, may have loaded the node
              // without building it. It is possible that the component include file
              // hasn't been included yet. See #2529246.
              webform_component_include($source_component['type']);

              // Load missing include files for conditional types.
              // In the case of the 'string', 'date', and 'time' conditional types, it is
              // not necessary to load their include files for conditional behavior
              // because the required functions are already loaded
              // in webform.conditionals.inc.
              switch ($conditional_type) {
                case 'numeric':
                  module_load_include('inc', 'webform', 'components/number');
                  break;
                case 'select':
                  module_load_include('inc', 'webform', 'components/select');
                  break;
              }

              $this->executionStackAccumulate($comparison_callback($source_values, $rule['value'], $source_component));

              // Record page number to later determine any intra-page dependency on this source.
              $source_page_nums[$source_component['page_num']] = $source_component['page_num'];
              break;
            case 'conditional_start':
              $this->executionStackPush($rule['operator']);
              break;
            case 'conditional_end':
              $this->executionStackAccumulate($this->executionStackPop());
              break;
          }
        }
        $conditional_result = $this->executionStackPop();

        foreach ($conditional['actions'] as $action) {
          $action_result = $action['invert'] ? !$conditional_result : $conditional_result;
          $target = $action['target'];
          $page_num = $components[$target]['page_num'];
          switch ($action['action']) {
            // Modification to original function done here.
            // Checks for the action type 'add_to'. The class itself ignores any
            // other action types.
            case 'add_to':
              if ($action_result && empty($targetLocked[$target])) {
                $input_values[$target][] = array(
                  'result' => $action['argument'],
                  'description' => $action['ruleset_description'],
                  'url' => $action['ruleset_url'],
                  'default' => FALSE,
                );
                $this->setMap[$page_num][$target] = TRUE;
              }
              break;
          }
        }
      } // End conditional loop.
    } // End page loop.

    return $input_values;
  }
}