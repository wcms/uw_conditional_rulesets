<?php
/**
 * Created by PhpStorm.
 * User: evnguyen
 * Date: 2/8/2018
 * Time: 1:36 PM
 * @file
 * Form elements and menu callbacks for the configuration page of Conditional
 * Rulesets module.
 */

/**
 * Handler for obtaining form.
 *
 * @param $node
 *
 * @return mixed
 */
function uw_conditional_rulesets_config_get_form($node) {
  $output = drupal_get_form('uw_conditional_rulesets_config_form');
  return $output;
}

/**
 * Form handler for the confiuration page of the module.
 *
 * @param $form
 * @param $form_state
 *
 * @return mixed
 */
function uw_conditional_rulesets_config_form($form, &$form_state) {
  $form['#tree'] = TRUE;
  if (!isset($form_state['uw_conditional_rulesets_add'])) {
    $form_state['uw_conditional_rulesets_add'] = FALSE;
  }

  $query = db_select('conditional_rulesets_configuration', 'crc')
    ->fields('crc')
    ->execute()
    ->fetchAllAssoc('config_group_id', PDO::FETCH_ASSOC);

  $form_state['latest_config_group_id'] = 0;

  // Scan for templates from this module, and any other modules which implements conditional_rulesets_preprocess.
  $templates = file_scan_directory(drupal_get_path('module', 'uw_conditional_rulesets') . '/templates', '/.tpl.php$/');
  foreach (module_implements('conditional_rulesets_preprocess') as $module) {
    $templates = array_merge($templates, file_scan_directory(drupal_get_path('module', $module) . '/templates', '/.tpl.php$/'));
  }

  // Build an array of template file names to use for the value of the options field.
  $options[0] = '-';
  foreach ($templates as $uri => $info) {
    $options[] = $info->filename;
  }
  $form_state['conditional_rulesets_templates'] = $options;

  // Build existing forms
  foreach ($query as $config_group_id => $config) {
    $form['uw_conditional_rulesets_config'][$config_group_id] = array(
      '#type' => 'fieldset',
      '#title' => t('Template & PDF settings'),
      '#collapsible' => TRUE,
    );

    $form['uw_conditional_rulesets_config'][$config_group_id]['uw_conditional_rulesets_nid'] = array(
      '#type' => 'textfield',
      '#title' => t('Target Node ID'),
      '#size' => 5,
      '#maxlength' => 5,
      '#description' => t('Target which webform.'),
      '#default_value' => isset($query[$config_group_id]['nid']) ? $query[$config_group_id]['nid'] : '',
    );

    $form['uw_conditional_rulesets_config'][$config_group_id]['uw_conditional_rulesets_template'] = array(
      '#type' => 'select',
      '#title' => t('Template file name'),
      '#description' => t('Specify a template name if needed.'),
      '#options' => $options,
      '#element_validate' => array('uw_conditional_rulesets_template_validate'),
      '#attributes' => array('cgid' => $config_group_id),
      '#default_value' => isset($query[$config_group_id]['template_file_name']) ? array_search($query[$config_group_id]['template_file_name'], $options) : FALSE,
    );

    $form['uw_conditional_rulesets_config'][$config_group_id]['uw_conditional_rulesets_fid'] = array(
      '#type' => 'textfield',
      '#title' => t('Form ID(s)'),
      '#description' => t('Specify which form(s) to use when generating the PDF. If more than one Form ID is used, separate using commas.'),
      '#default_value' => isset($query[$config_group_id]['fid']) ? $query[$config_group_id]['fid'] : '',
    );

    $form['uw_conditional_rulesets_config'][$config_group_id]['uw_conditional_rulesets_delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#name' => $config_group_id,
      '#submit' => array('uw_conditional_rulesets_config_delete'),
    );

    if ($form_state['latest_config_group_id'] < $config_group_id) {
      $form_state['latest_config_group_id'] = $config_group_id;
    }
  }

  // Build empty form if any
  $id = $form_state['latest_config_group_id'] + 1;
  if ($form_state['uw_conditional_rulesets_add']) {
    $form['uw_conditional_rulesets_config'][$id] = array(
      '#type' => 'fieldset',
      '#title' => t('Template & PDF settings'),
      '#collapsible' => TRUE,
    );

    $form['uw_conditional_rulesets_config'][$id]['uw_conditional_rulesets_nid'] = array(
      '#type' => 'textfield',
      '#title' => t('Target Node ID'),
      '#size' => 5,
      '#maxlength' => 5,
      '#description' => t('Specify which Webform to target for templating and form use.'),
      '#default_value' => FALSE,
    );

    $form['uw_conditional_rulesets_config'][$id]['uw_conditional_rulesets_template'] = array(
      '#type' => 'select',
      '#title' => t('Template file name'),
      '#description' => t('Specify a template name, if needed.'),
      '#options' => $options,
      '#default_value' => FALSE,
      '#element_validate' => array('uw_conditional_rulesets_template_validate'),
      '#attributes' => array('cgid' => $id),
    );

    $form['uw_conditional_rulesets_config'][$id]['uw_conditional_rulesets_fid'] = array(
      '#type' => 'textfield',
      '#title' => t('Form ID(s)'),
      '#description' => t('Specify which form(s) to use when generating the PDF. If more than one Form ID is used, separate using commas.'),
      '#default_value' => FALSE,
    );

    $form['uw_conditional_rulesets_config'][$id]['uw_conditional_rulesets_delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#name' => t('new_delete'),
      '#submit' => array('uw_conditional_rulesets_config_delete'),
    );
  }

  $form['uw_conditional_rulesets_config']['add'] = array(
    '#type' => 'submit',
    '#value' => t('Add new configuration'),
    '#submit' => array('uw_conditional_rulesets_config_add'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => array('uw_conditional_rulesets_config_submit'),
  );

  return $form;
}

/**
 * Validate handler function.
 * A template file must have a node ID specified before it can be applied.
 *
 * @param $element
 * @param $form
 * @param $form_state
 */
function uw_conditional_rulesets_template_validate($element, $form, &$form_state) {
  $config_group_id = $element['#attributes']['cgid'];
  if (($form_state['uw_conditional_rulesets_config'][$config_group_id]['uw_conditional_rulesets_nid']['#value'] == '' ||
      $form_state['uw_conditional_rulesets_config'][$config_group_id]['uw_conditional_rulesets_nid']['#value'] == NULL) &&
      $element['#value'] !== '0') {
    form_error($element, t('Cannot use a template file if node ID is empty'));
  }
}

/**
 * Add submit handler for adding another configuration group.
 *
 * @param $form
 * @param $form_state
 */
function uw_conditional_rulesets_config_add($form, &$form_state) {
  $form_state['uw_conditional_rulesets_add'] = TRUE;
  $form_state['rebuild'] = TRUE;
}

/**
 * Delete submit handler for deleting a configuration group.
 *
 * @param $form
 * @param $form_state
 */
function uw_conditional_rulesets_config_delete($form, &$form_state) {
  if ($form_state['triggering_element']['#name'] == 'new_delete') {
    $form_state['uw_conditional_rulesets_add'] = FALSE;
  }
  else {
    $config_group_id = $form_state['triggering_element']['#name'];
    db_delete('conditional_rulesets_configuration')
      ->condition('config_group_id', $config_group_id)
      ->execute();
  }
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler for the configuration group.
 *
 * @param $form
 * @param $form_state
 */
function uw_conditional_rulesets_config_submit($form, &$form_state) {
  $query = db_select('conditional_rulesets_configuration', 'crc')
    ->fields('crc')
    ->execute()
    ->fetchAllAssoc('config_group_id', PDO::FETCH_ASSOC);

  // First update any existing entries.
  foreach ($query as $config_group_id => $config) {
    $template_name = $form_state['conditional_rulesets_templates'][$form_state['values']['uw_conditional_rulesets_config'][$config_group_id]['uw_conditional_rulesets_template']];
    if ($form_state['values']['uw_conditional_rulesets_config'][$config_group_id]['uw_conditional_rulesets_nid'] !== $query[$config_group_id]['nid'] ||
        $template_name !== $query[$config_group_id]['template_file_name'] ||
        $form_state['values']['uw_conditional_rulesets_config'][$config_group_id]['uw_conditional_rulesets_fid'] !== $query[$config_group_id]['fid']) {

      $record['config_group_id'] = $config_group_id;
      $record['nid'] = $form_state['values']['uw_conditional_rulesets_config'][$config_group_id]['uw_conditional_rulesets_nid'];
      $record['template_file_name'] = $template_name;
      $record['fid'] = $form_state['values']['uw_conditional_rulesets_config'][$config_group_id]['uw_conditional_rulesets_fid'];
      drupal_write_record('conditional_rulesets_configuration', $record, 'config_group_id');
    }
  }

  // Then save the new entry if it exists.
  if ($form_state['uw_conditional_rulesets_add']) {
    $id = $form_state['latest_config_group_id'] + 1;
    $record['config_group_id'] = $id;
    $record['nid'] = $form_state['values']['uw_conditional_rulesets_config'][$id]['uw_conditional_rulesets_nid'];
    $template_name = $form_state['conditional_rulesets_templates'][$form_state['values']['uw_conditional_rulesets_config'][$id]['uw_conditional_rulesets_template']];
    $record['template_file_name'] = $template_name;
    $record['fid'] = $form_state['values']['uw_conditional_rulesets_config'][$id]['uw_conditional_rulesets_fid'];
    drupal_write_record('conditional_rulesets_configuration', $record);
    $form_state['uw_conditional_rulesets_add'] = FALSE;
  }
  drupal_theme_rebuild();
}