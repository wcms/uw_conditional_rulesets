<?php

/**
 * @file
 * Extends te conditionals form page.
 */

/**
 * Extends webform_conditional_insert and inserts a new ruleset into the database.
 * @param $conditional
 */
function uw_conditional_rulesets_webform_insert($conditional) {
  foreach ($conditional['actions'] as $aid => $action) {
    $ruleset['nid'] = $conditional['nid'];
    $ruleset['rgid'] = $conditional['rgid'];
    $ruleset['aid'] = $aid;
    $ruleset['ruleset_result'] = $action['argument'];
    $ruleset['ruleset_description'] = $action['ruleset_description'];
    $ruleset['ruleset_url'] = $action['ruleset_url'];
    drupal_write_record('conditional_rulesets_data', $ruleset);
  }
}

/**
 * Helper function that deletes the rgid entry from the conditional_rulesets_data table.
 * @param $node
 * @param $conditional
 */
function uw_conditional_rulesets_webform_delete($node, $conditional) {
  db_delete('conditional_rulesets_data')
    ->condition('nid', $node->nid)
    ->condition('rgid', $conditional['rgid'])
    ->execute();
}

/**
 * Submit handler for adding a new conditional.
 * Extends webform_conditionals_form_submit and adds extra fields to actions.
 * @param $form
 * @param $form_state
 */
function _uw_conditional_rulesets_webform_form_add($form, &$form_state) {
  foreach ($form_state['values']['conditionals'] as $cid => $aconditional_group) {
    foreach ($form_state['values']['conditionals'][$cid]['actions'] as $aid => $action) {
      $form_state['values']['conditionals'][$cid]['actions'][$aid]['ruleset_description'] = NULL;
      $form_state['values']['conditionals'][$cid]['actions'][$aid]['ruleset_url'] = NULL;
    }
  }
  $form_state['rebuild'] = TRUE;
}

/**
 * Additional Form API #process function to add extra textfields to a webform conditional
 * element.
 * @param $element
 *
 * @return mixed
 */
function _uw_conditional_rulesets_webform_process($element) {
  foreach ($element['#default_value']['actions'] as $aid => $action) {
    $element['actions'][$aid]['ruleset_description'] = array(
      '#type' => 'textarea',
      '#title' => t('Ruleset Description @id', array('@id' => ($aid+1))),
      '#maxlength' => NULL,
      '#default_value' => isset($action['ruleset_description']) ? $action['ruleset_description'] : '',
      //'#cols' => 30,
      '#rows' => 5,
    );
    $element['actions'][$aid]['ruleset_url'] = array(
      '#type' => 'textfield',
      '#title' => t('Ruleset url @id', array('@id' => ($aid+1))),
      '#maxlength' => NULL,
      '#default_value' => isset($action['ruleset_url']) ? $action['ruleset_url'] : '',
    );
  }
  return $element;
}

/**
 * Execute webform conditionals that uses 'add_to" action.
 * @param $node
 * @param $data
 *
 * @return mixed: An array of values
 */
function uw_conditional_rulesets_execute_conditionals($node, $data) {
  module_load_include('inc', 'uw_conditional_rulesets', $name = 'includes/uw_conditional_rulesets.rulesetconditionals');
  $sorter = RulesetConditionals::factory($node);
  $sorter->reportErrors();
  $input_values = $data;
  $input_values = $sorter->executeRulesetConditionals($input_values);
  return $input_values;
}

/**
 * Helper function that will delete an rule group if no actions exists after component
 * deletion.
 * @param $conditional
 *
 * @return mixed
 */
function uw_conditional_rulesets_empty_ruleset_actions(&$conditional) {
  // If the rule group doesn't actually exist, then delete the useless ruleset actions.
  if (!isset($conditional['rgid'])) {
    foreach ($conditional['actions'] as $aid => $action) {
      foreach ($action as $key => $value) {
        switch ($key) {
          case 'ruleset_description':
            unset($conditional['actions'][$aid][$key]);
            break;
          case 'ruleset_url':
            unset($conditional['actions'][$aid][$key]);
            break;
        }
      }
    }
  }
  return $conditional;
}

/**
 * Mimics very closely to webform_conditionals_form_validate.
 * Validate handler for webform_conditionals_form().
 * Modifies webform_coditionals_form_validate to add extra checks for the 'add_to' action.
 *
 * @param $form
 * @param $form_state
 */
function _uw_conditional_rulesets_webform_form_validate($form, &$form_state) {
  // Skip validation unless this is saving the form.
  $button_key = end($form_state['triggering_element']['#array_parents']);
  if ($button_key !== 'submit') {
    return;
  }

  $node = $form['#node'];
  $components = $node->webform['components'];
  $component_options = webform_component_options();
  foreach ($form_state['complete form']['conditionals'] as $conditional_key => $element) {
    if (substr($conditional_key, 0, 1) !== '#' && $conditional_key !== 'new') {
      $conditional = $element['conditional'];
      $targets = array();
      foreach ($conditional['actions'] as $action_key => $action) {

        if (is_numeric($action_key)) {
          $operation = $action['action']['#value'];
          $target_id = $action['target']['#value'];
          // Modification done here.
          // If the target is a Ruleset Category, then do not allow for the isn't option.
          $invert = $action['invert']['#value'];

          if ($invert === '1' && $components[$action['target']['#value']]['type'] === 'ruleset_category') {
            form_set_error('conditionals][' . $conditional_key . '][actions][' . $action_key . '][invert',
              t('%target cannot use the "isn\'t" option.',
                array('%target' => $components[$action['target']['#value']]['name'])));
          }

          // Modification done here.
          // If the operation is 'add_to', the allow for multiple actions of the same component.
          if (isset($targets[$target_id][$operation]) && $operation !== 'add_to') {
            form_set_error('conditionals][' . $conditional_key . '][actions][' . $action_key . '][target',
              t('A operation %op cannot be made for a component more than once. (%target).',
                array('%op' => $action['action']['#options'][$operation],
                  '%target' => $components[$action['target']['#value']]['name'])));
          }

          $component_type = $node->webform['components'][$action['target']['#value']]['type'];
          if (!webform_conditional_action_able($component_type, $action['action']['#value'])) {
            form_set_error('conditionals][' . $conditional_key . '][actions][' . $action_key . '][action',
              t('A component of type %type can\'t be %action. (%target)',
                array(
                  '%action' => $action['action']['#options'][$action['action']['#value']],
                  '%type' => $component_options[$component_type],
                  '%target' => $components[$action['target']['#value']]['name'])));
          }
          $targets[$target_id][$operation] = $target_id;
        }
      }

      foreach ($conditional['rules'] as $rule_key => $rule) {
        // Validate component rules, but not conditional_start/end rules.
        if (is_numeric($rule_key) && $rule['source_type']['#value'] == 'component' && isset($targets[$rule['source']['#value']])) {
          form_set_error('conditionals][' . $conditional_key . '][rules][' . $rule_key . '][source',
            t('The subject of the conditional cannot be the same as the component that is changed (%target).',
              array('%target' => $components[$rule['source']['#value']]['name'])));
        }
      }
    }
  }

  // Form validation will not rebuild the form, so we need to ensure
  // necessary JavaScript will still exist.
  _webform_conditional_expand_value_forms($node);
}