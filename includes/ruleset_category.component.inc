<?php
/**
 * Created by PhpStorm.
 * User: evnguyen
 * Date: 1/18/2018
 * Time: 10:12 AM
 * @file
 * Ruleset category Webform component.
 */

/**
 * Implements _webform_defaults_component().
 *
 * @return array
 */
function _webform_defaults_ruleset_category() {
  return array(
    'name' => '',
    'form_key' => NULL,
    'pid' => 0,
    'weight' => 0,
    'value' => '',
    'mandatory' => 0,
    'extra' => array(
      'private' => FALSE,
      'number_of_results' => '1',
      'result' => '',
      'description' => '',
      'url' => '',
    ),
  );
}

/**
 * Implements _webform_theme_component().
 * @return array
 */
function _webform_theme_ruleset_category() {
  return array(
    'webform_display_ruleset_category' => array(
      'render element' => 'element',
    ),
  );
}

/**
 * Implements _webform_edit_component().
 * @param $component
 *
 * @return array
 */
function _webform_edit_ruleset_category($component) {
  $form = array();
  $form['extra'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default Values'),
    '#weight' => 0,
    '#description' => t('Specify the default ruleset below, as well as the number of results that will be pulled from this category. The default ruleset will be used as filler whenever there is not enough results to pull from the ruleset category. On save, tokens will be formed using the Form Key. The number of tokens formed by this category will be 3 times the number of results specified.'),
  );

  $form['extra']['number_of_results'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of Results'),
    '#required' => TRUE,
    '#default_value' => $component['extra']['number_of_results'],
    '#description' => t('Enter the number of results that will need to be pulled from this ruleset category.'),
    '#size' => 2,
  );

  $form['extra']['result'] = array(
    '#type' => 'textfield',
    '#title' => t('Result'),
    '#required' => TRUE,
    '#default_value' => $component['extra']['result'],
    '#description' => t('The default result text of the default ruleset.'),
  );

  $form['extra']['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $component['extra']['description'],
    '#description' => t('The default description text of the default ruleset.'),
  );

  $form['extra']['url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#default_value' => $component['extra']['url'],
    '#description' => t('The default URL of the default ruleset.'),
  );
  return $form;
}

/**
 * Implements _webform_render_component().
 */
function _webform_render_ruleset_category($component, $value = NULL, $filter = TRUE, $submission = NULL) {
  $element = array(
    '#title' => $filter ? NULL : $component['name'],
    '#weight' => $component['weight'],
    '#translatable' => array('title'),
  );
  return $element;
}

/**
 * Format output of data for ruleset category.
 * @param $variables
 *
 * @return string
 */
function theme_webform_display_ruleset_category($variables) {
  $element = $variables['element'];
  $output = '';
  $prefix = $element['#format'] == 'html' ? '' : $element['#field_prefix'];
  $suffix = $element['#format'] == 'html' ? '' : $element['#field_suffix'];

  foreach ($element['#value'] as $index => $ruleset) {
    $output .= $prefix;
    $output .= $element['#format'] == 'html' ? check_plain($ruleset['result']) : $ruleset['result'];
    $output .= $suffix . '<br>';
  }

  return $output;
}

/**
 * Implements _webform_display_component().
 */
function _webform_display_ruleset_category($component, $value, $format = 'html', $submission = array()) {
  if (!empty($submission)) {
    $nid = $submission->nid;
    $sid = $submission->sid;
    $results = db_select('conditional_rulesets_results', 'cpr')
      ->fields('cpr')
      ->condition('sid', $sid)
      ->condition('nid', $nid)
      ->execute()
      ->fetchAllAssoc('cid', PDO::FETCH_ASSOC);
  }

  $element = array(
    '#title' => $component['name'],
    '#markup' => isset($value[0]) ? $value[0] : NULL,
    '#weight' => $component['weight'],
    '#format' => $format,
    '#theme' => 'webform_display_ruleset_category',
    '#theme_wrappers' => $format == 'text' ? array('webform_element_text') : array('webform_element'),
    '#translatable' => array('title'),
    '#value' => isset($results)? json_decode($results[$component['cid']]['rulesets'], TRUE) : ' ',
  );

  return $element;
}