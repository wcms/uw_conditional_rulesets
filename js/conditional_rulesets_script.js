/**
 * @file
 * Javascript to enhance webform conditional UI.
 */
(function ($) {

  "use strict";

  Drupal.behaviors.convertBehaviors = {
    attach: function (context) {
      $('.webform-conditional-ruleset label').css('font-size', '1rem');
      var $context = $(context);
      $context.find('#webform-conditionals-ajax:not(.webform-conditional-processed)')
        .bind('change', function (e) {
          var $target = $(e.target);
          if ($target.is('.webform-conditional-action select')) {
            var $argument = $target.parents('.webform-conditional-condition:first').find('.webform-conditional-argument input');
            var $ruleset = $target.parents('.webform-conditional-condition:first').find('.webform-conditional-ruleset');
            var $action = $target.val();
            var argShown = $argument.is(':visible');
            var rulesetShown = $ruleset.is(':visible');
            switch ($action) {
              case 'show':
              case 'require':
                if (argShown) {
                  $argument.hide();
                }
                if (rulesetShown) {
                  $ruleset.hide();
                }
                break;
              case 'set':
                if (!argShown) {
                  $argument.show();
                }
                if (rulesetShown) {
                  $ruleset.hide();
                }
                break;
              case 'add_to':
                if (!argShown) {
                  $argument.show();
                }
                if (!rulesetShown) {
                  $ruleset.show();
                }
                break;
            }
          }
        });
      $context.find('.webform-conditional-action select').trigger('change');
    }//End attach.
  };
}(jQuery));
