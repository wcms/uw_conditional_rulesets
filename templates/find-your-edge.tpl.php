<?php

/**
 * @file
 * Customize confirmation screen after successful submission.
 *
 * This file may be renamed "webform-confirmation-[nid].tpl.php" to target a
 * specific webform e-mail on your site. Or you can leave it
 * "webform-confirmation.tpl.php" to affect all webform confirmations on your
 * site.
 *
 * Available variables:
 * - $node: The node object for this webform.
 * - $progressbar: The progress bar 100% filled (if configured). This may not
 *   print out anything if a progress bar is not enabled for this node.
 * - $confirmation_message: The confirmation message input by the webform
 *   author.
 * - $sid: The unique submission ID of this submission.
 * - $url: The URL of the form (or for in-block confirmations, the same page).

 *
 * For the find-your-edge template to work properly (i.e. populate results) the form keys of the
 * ruleset category needs to named accordingly.
 *
 * You will need to use these exact form keys (case sensitive):
 *  skills_identification_and_articulation_workshop
 *  career_development_course
 *  work_and_community_experiences
 *  pd_courses
 *  capstone_workshop
 *
 * Make sure that the correct number of results pulled is inputted into the
 * each field for each ruleset category. Work and Community Experiences must have a value
 * of 3 for the number of results. There is value less than 3, you will generate an
 * error on the results page.
 *
 * The form id for non-international students should be placed first,
 * followed by the one for international students.
 * For example: Form ID(s) = 1,2
 * Where 1 is the form id for non-international students and 2 is the form id for
 * international students.
 */

?>

<?php
/**
 * Helper function that checks if a string contains a number. Returns false
 * otherwise.
 *
 * @param $string
 *
 * @return bool
 */
  function has_number($string) {
    $len = drupal_strlen($string);
    for ($i = 0; $i < $len; $i++) {
      if(ctype_digit($string[$i])) {
        return TRUE;
      }
    }
    return FALSE;
  }

/**
 * Helper function to check if string is a course code
 * This assumes all course code will always start with a letter and end with either
 * a number or an uppercase letter. Furthermore, every course code will have at
 * least one number.
 *
 * @param $string
 *
 * @return bool
 */
  function is_course($string) {
    $first = $string[0];
    $last = $string[drupal_strlen($string) - 1];

    if (has_number($string) && ctype_alpha($first) && (ctype_digit($last) || ctype_upper($last))) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

/**
 * Helper function to check if a string is a PD course
 * This assumes that every PD course will start with "PD" and the last character
 * of the string is a number.
 *
 * @param $string
 *
 * @return bool
 */
  function is_pd_course($string) {
    $pd = $string[0] . $string[1];
    $last = $string[drupal_strlen($string) - 1];
    return (ctype_digit($last) && $pd == "PD");
  }

/**
 * Helper function to check if position is a Don position.
 *
 * @param $string
 *
 * @return bool
 */
  function is_don_position($string) {
    $length = drupal_strlen($string);
    $word = "";
    for ($i = 4; $i > 0; $i--) {
      $word .= $string[$length - $i];
    }
    return ($word == " Don" || $word == " don");
  }

/**
 * Helper function which filters the list into a list with only one Don position.
 *
 * @param $list
 *
 * @return array
 */
  function filter_don($list) {
    $length = count($list);

    for ($i = 0; $i < $length; $i++) {
      if (is_don_position($list[$i]->result)) {
        unset($list[$i]);
        // Reindex the keys in array.
        $list = array_values($list);
        $length--;
      }
    }
    return $list;
  }

/**
 * Checks if user is an international student
 * Will assume non-international until sees "YESINTL"
 *
 * @param $data
 * @param $rule
 *
 * @return bool
 */
function is_international($data) {
  $length = count($data);
  // Reindex array since page breaks will break the indexing sequence.
  $data = array_values($data);
  for ($i = 0; $i < $length; $i++) {
    if (isset($data[$i])) {
      $number_of_selections = count($data[$i]);
      for ($j = 0; $j < $number_of_selections; $j++) {
        if($data[$i][$j] == 'YESINTL') {
          return true;
        }
      }
    }
  }
  return false;
}

/**
 * Provides the start <a> tag if $string is not "Other Experience".
 *
 * @param $string
 * @param $link
 */
  function gen_href_start($string, $link) {
    if ($string != "Other Experience") {
      print '<a href="' . $link . '" target="_blank">';
    }
  }

/**
 * Provides the end <a> tag if $string is not "Other Experience".
 *
 * @param $string
 */
  function gen_href_end($string) {
    if ($string != "Other Experience") {
      print '</a>';
    }
  }

/**
 * Process the string so that it can properly added to a pdf template.
 *
 * @param $string
 *
 * @return mixed
 */
  function pdf_process($string) {
    $string = preg_replace('~<a href=\s*".*"\s*>~', '', $string);
    $string = preg_replace('~</a>~', '', $string);
    return $string;
  }

/**
 * After processing the results, update the database to hold the new results.
 * This allows the tokens to have the updated results so the FillPDFs using the tokens
 * will have the correct values.
 *
 * @param $node
 * @param $sid
 * @param $component1
 * @param $component2
 * @param $component3
 * @param $component_pd
 * @param $component4
 */
  function pdf_store_results($node, $sid, $component1, $component2, $component3, $component_pd, $component4) {

    // If for some reason the Webform is set up wrong, return immediately to avoid errors.
    if (!isset($component1['cid']) || !isset($component2['cid']) || !isset($component3['cid']) ||
        !isset($component_pd['cid']) || !isset($component4['cid'])) {
      return;
    }

    // Store component 1 info.
    foreach ($component1['rulesets'] as $index => $ruleset) {
      $component1['rulesets'][$index]['description'] = pdf_process($ruleset['description']);
    }
    db_update('conditional_rulesets_results')
      ->fields(array(
        'nid' => $node->nid,
        'sid' => $sid,
        'cid' => $component1['cid'],
        'rulesets' => json_encode($component1['rulesets'])
      ))
      ->condition('nid', $node->nid)
      ->condition('sid', $sid)
      ->condition('cid', $component1['cid'])
      ->execute();

    // Store component 2 info.
    foreach ($component2['rulesets'] as $index => $ruleset) {
      $component2['rulesets'][$index]['description'] = pdf_process($ruleset['description']);
    }
    db_update('conditional_rulesets_results')
      ->fields(array(
        'nid' => $node->nid,
        'sid' => $sid,
        'cid' => $component2['cid'],
        'rulesets' => json_encode($component2['rulesets'])
      ))
      ->condition('nid', $node->nid)
      ->condition('sid', $sid)
      ->condition('cid', $component2['cid'])
      ->execute();

    // Store component 3 info.
    foreach ($component3['rulesets'] as $index => $ruleset) {
      $component3['rulesets'][$index]['description'] = pdf_process($ruleset['description']);
    }
    db_update('conditional_rulesets_results')
      ->fields(array(
        'nid' => $node->nid,
        'sid' => $sid,
        'cid' => $component3['cid'],
        'rulesets' => json_encode($component3['rulesets'])
      ))
      ->condition('nid', $node->nid)
      ->condition('sid', $sid)
      ->condition('cid', $component3['cid'])
      ->execute();

    // Store component_pd info.
    foreach ($component_pd['rulesets'] as $index => $ruleset) {
      $component_pd['rulesets'][$index]['description'] = pdf_process($ruleset['description']);
    }
    db_update('conditional_rulesets_results')
      ->fields(array(
        'nid' => $node->nid,
        'sid' => $sid,
        'cid' => $component_pd['cid'],
        'rulesets' => json_encode($component_pd['rulesets'])
      ))
      ->condition('nid', $node->nid)
      ->condition('sid', $sid)
      ->condition('cid', $component_pd['cid'])
      ->execute();

    // Store component4 info.
    foreach ($component4['rulesets'] as $index => $ruleset) {
      $component4['rulesets'][$index]['description'] = pdf_process($ruleset['description']);
    }
    db_update('conditional_rulesets_results')
      ->fields(array(
        'nid' => $node->nid,
        'sid' => $sid,
        'cid' => $component4['cid'],
        'rulesets' => json_encode($component4['rulesets'])
      ))
      ->condition('nid', $node->nid)
      ->condition('sid', $sid)
      ->condition('cid', $component4['cid'])
      ->execute();
  }

/**
 * FIll the list of rulesets only if there is not enough to pull from.
 * If there is still enough to pull from after removing the default result,
 * then just remove the default ruleset.
 *
 * @param $node
 * @param $cid
 * @param $rulesets
 *
 * @return mixed
 */
  function find_your_edge_append_default_values($node, $cid, $rulesets) {
    // First find the default ruleset.
    $default = array();
    $default_index = 0;
    foreach ($rulesets['rulesets'] as $index => $ruleset) {
      if ($ruleset['default']) {
        $default = $ruleset;
        $default_index = $index;
        break;
      }
    }

    // Remove the default ruleset first.
    unset($rulesets['rulesets'][$default_index]);

    // If there is not enough to pull without default, fill with default rulesets.
    $number_of_rulesets = count($rulesets['rulesets']);
    if ($number_of_rulesets < $node->webform['components'][$cid]['extra']['number_of_results']) {
      $difference = $node->webform['components'][$cid]['extra']['number_of_results'] - $number_of_rulesets;
      for ($i = 0; $i < $difference; $i++) {
        $rulesets['rulesets'][] = $default;
      }
    }

    return $rulesets;
  }

/**
 * Obtain the list of rulesets for a given ruleset category.
 *
 * @param $node
 * @param $cid
 * @param $rulesets
 *
 * @return array
 */
  function get_component_results($node, $cid, $rulesets) {
    $results_list = array();
    $results_list['cid'] = $cid;

    // Determine if there is enough to pull.
    $rulesets = find_your_edge_append_default_values($node, $cid, $rulesets);

    // Start pulling values.
    for ($i = 0; $i < $node->webform['components'][$cid]['extra']['number_of_results']; $i++) {
      $element = pull_random_element($rulesets['rulesets']);
      $index = array_search($element, $rulesets['rulesets']);
      unset($rulesets['rulesets'][$index]);
      $results_list['rulesets'][] = $element;
    }
    return $results_list;
  }

/**
 * Similar to get_component_results except remove extra "Don" positions if necessary.
 *
 * @param $node
 * @param $cid
 * @param $rulesets
 *
 * @return array
 */
  function get_component_exp_results($node, $cid, $rulesets) {
    $results_list = array();
    $results_list['cid'] = $cid;

    // Determine if there is enough to pull.
    $rulesets = find_your_edge_append_default_values($node, $cid, $rulesets);

    // Start pulling values.
    for ($i = 0; $i < $node->webform['components'][$cid]['extra']['number_of_results']; $i++) {
      $element = pull_random_element($rulesets['rulesets']);
      $index = array_search($element, $rulesets['rulesets']);
      unset($rulesets['rulesets'][$index]);
      $results_list['rulesets'][] = $element;
      // If we've found a Don position, filter out the rest so we can't get another.
      // This should only be true at most ONCE.
      if (is_don_position($results_list['rulesets'][$i]['result'])) {
        $rulesets['rulesets'] = filter_don($results_list['rulesets']);
      }
    }

    return $results_list;
  }


/**
 * Checks if all the components have been properly populated with rulesets.
 * If not, fill with empty rulesets in order to avoid errors displaying, while also
 * indicating that something is wrong.
 *
 * @param $component1
 * @param $component2
 * @param $component3
 * @param $component_pd
 * @param $component4
 */
  function validate(&$component1, &$component2, &$component3, &$component_pd, &$component4) {
    $empty_ruleset = array(
      'result' => '',
      'description' => '',
      'url' => '',
    );
    $component1['rulesets'][0] = !isset($component1['rulesets'][0]) ? $empty_ruleset : $component1['rulesets'][0];
    $component2['rulesets'][0] = !isset($component2['rulesets'][0]) ? $empty_ruleset : $component2['rulesets'][0];
    $component3['rulesets'][0] = !isset($component3['rulesets'][0]) ? $empty_ruleset : $component3['rulesets'][0];
    $component3['rulesets'][1] = !isset($component3['rulesets'][1]) ? $empty_ruleset : $component3['rulesets'][1];
    $component3['rulesets'][2] = !isset($component3['rulesets'][2]) ? $empty_ruleset : $component3['rulesets'][2];
    $component_pd['rulesets'][0] = !isset($component_pd['rulesets'][0]) ? $empty_ruleset : $component_pd['rulesets'][0];
    $component4['rulesets'][0] = !isset($component4['rulesets'][0]) ? $empty_ruleset : $component4['rulesets'][0];
  }

  $submission = webform_get_submission($node->nid, $sid);
  $access_token = token_replace('[submission:access-token]', array('webform-submission' => $submission));

  $configurations = db_select('conditional_rulesets_configuration', 'crc')
    ->fields('crc')
    ->condition('nid', $node->nid)
    ->condition('template_file_name', 'find-your-edge.tpl.php')
    ->execute()
    ->fetchAssoc();

  $unprocessed_results = db_select('conditional_rulesets_unprocessed_results', 'crpr')
    ->fields('crpr')
    ->condition('sid', $sid)
    ->condition('nid', $node->nid)
    ->execute()
    ->fetchAllAssoc('cid', PDO::FETCH_ASSOC);

  foreach ($unprocessed_results as $cid => $value) {
    $unprocessed_results[$cid]['rulesets'] = drupal_json_decode($unprocessed_results[$cid]['rulesets']);
  }
  unset($unprocessed_results['nid']);
  unset($unprocessed_results['sid']);

  $fids = explode(',', str_replace(' ', '', $configurations['fid']));
  $component1 = array();
  $component2 = array();
  $component3 = array();
  $component_pd = array();
  $component4 = array();

  foreach ($unprocessed_results as $cid => $info) {
    $form_key = $node->webform['components'][$cid]['form_key'];
    switch ($form_key) {
      case 'skills_identification_and_articulation_workshop':
        $component1 = get_component_results($node, $cid, $info);
        break;

      case 'career_development_course':
        $component2 = get_component_results($node, $cid, $info);
        break;

      case 'work_and_community_experiences':
        $component3 = get_component_exp_results($node, $cid, $info);
        break;

      case 'pd_courses':
        $component_pd = get_component_results($node, $cid, $info);
        break;

      case 'capstone_workshop':
        $component4 = get_component_results($node, $cid, $info);
        break;
    }
  }

  // Avoid offset errors in case webform is set up incorrectly.
  validate($component1, $component2, $component3, $component_pd, $component4);

  // Store results for PDF use.
  pdf_store_results($node, $sid, $component1, $component2, $component3, $component_pd, $component4);

  $international = false;
  if (is_international($submission->data)) {
    $international = true;
  }

  drupal_add_css(drupal_get_path('module', 'uw_conditional_rulesets') .
    '/conditional_rulesets_style.css', array('group' => CSS_DEFAULT, 'weight' => 99));
?>
<div class="flex-container">
  <div class="flex-message">
    <p>We’ve created the following list of EDGE courses, workshops and opportunities
      based on your responses. You can complete these milestones during any term
      and in almost any order you choose. It may be possible for you to complete
      EDGE with an entirely different set of milestones — these are just options.
      The choice is ultimately yours. Each milestone will provide a link for further information in a separate tab.</p>
    <p>If you're ready to take the next step, you can register for EDGE by submitting the
      <a href="https://uwaterloo.ca/edge/registration-form" target="_blank">registration form</a>
      available on our website. If you have any questions or concerns about the registration process,
      contact EDGE instructional support coordinator Ben McDonald at
      <a href="mailto:ben.mcdonald@uwaterloo.ca">ben.mcdonald@uwaterloo.ca.</a>
    </p>
  </div>

  <div class="flex-component-title margin-top">
    <h2 class="edge-header">Skills Identification and Articulation Workshop</h2>
</div>

  <div class="flex-component-block">
    <div class="component_square">
      <div class="call-to-action-top-wrapper">
      <?php gen_href_start($component1['rulesets'][0]['result'], $component1['rulesets'][0]['url']) ?>
        <div class="call-to-action-wrapper">
          <div class="call-to-action-theme-uWaterloo">
            <div class="call-to-action-big-text"> <?php print $component1['rulesets'][0]['result'] ?> </div>
          </div>
        </div>
      <?php gen_href_end($component1['rulesets'][0]['result']) ?>
      </div>
    </div>
  </div>

  <div class="flex-component-description">
    <div>
      <?php print $component1['rulesets'][0]['description']?>
    </div>
  </div>

  <div class="flex-component-title margin-top">
    <h2 class="edge-header">Career Development Course</h2>
  </div>

  <div class="flex-component-block">
    <div class="component_square">
      <div class="call-to-action-top-wrapper">
        <?php gen_href_start($component2['rulesets'][0]['result'], $component2['rulesets'][0]['url']); ?>
          <div class="call-to-action-wrapper">
            <div class="call-to-action-theme-uWaterloo">
              <div class="call-to-action-big-text"> <?php print $component2['rulesets'][0]['result']; ?> </div>
            </div>
          </div>
        <?php gen_href_end($component2['rulesets'][0]['result']); ?>
      </div>
    </div>
  </div>

  <div class="flex-component-description">
    <div>
      <?php print $component2['rulesets'][0]['description']; ?>
    </div>
  </div>

   <div class="flex-component-title margin-top">
    <h2 class="edge-header">Work and Community Experiences</h2>
  </div>

  <div class="flex-component-block">
    <div class="component_square">
      <div class="call-to-action-top-wrapper">
        <?php gen_href_start($component3['rulesets'][0]['result'], $component3['rulesets'][0]['url']);?>
          <div class="call-to-action-wrapper">
            <div class="call-to-action-theme-uWaterloo">
              <div class="call-to-action-big-text"> <?php print $component3['rulesets'][0]['result'] ?> </div>
            </div>
          </div>
        <?php gen_href_end($component3['rulesets'][0]['result']); ?>
      </div>
    </div>
  </div>

  <div class="flex-component-description">
    <div>
      <?php print $component3['rulesets'][0]['description'] ?>
    </div>
  </div>

  <div class="flex-component-block">
    <div class="component_square">
      <div class="call-to-action-top-wrapper">
        <?php gen_href_start($component3['rulesets'][1]['result'], $component3['rulesets'][1]['url']); ?>
          <div class="call-to-action-wrapper">
            <div class="call-to-action-theme-uWaterloo">
              <div class="call-to-action-big-text"> <?php print $component3['rulesets'][1]['result'] ?> </div>
            </div>
          </div>
        <?php gen_href_end($component3['rulesets'][1]['result']); ?>
      </div>
    </div>
  </div>

  <div class="flex-component-description">
    <div>
      <?php print $component3['rulesets'][1]['description'] ?>
    </div>
  </div>

  <div class="flex-component-block">
    <div class="component_square">
      <div class="call-to-action-top-wrapper">
        <?php gen_href_start($component3['rulesets'][2]['result'], $component3['rulesets'][2]['url']); ?>
          <div class="call-to-action-wrapper">
            <div class="call-to-action-theme-uWaterloo">
              <div class="call-to-action-big-text"> <?php print $component3['rulesets'][2]['result'] ?> </div>
            </div>
          </div>
        <?php gen_href_end($component3['rulesets'][2]['result']); ?>
      </div>
    </div>
  </div>

  <div class="flex-component-description">
    <div>
      <?php print $component3['rulesets'][2]['description'] ?>
    </div>
  </div>

  <div id="pd-block" class="flex-component-block">
    <div class="component_square">
      <div class="call-to-action-top-wrapper">
        <?php gen_href_start($component_pd['rulesets'][0]['result'], $component_pd['rulesets'][0]['url']); ?>
        <div class="call-to-action-wrapper">
          <div class="call-to-action-theme-uWaterloo">
            <div class="call-to-action-big-text"> <?php print $component_pd['rulesets'][0]['result'] ?> </div>
          </div>
        </div>
        <?php gen_href_end($component_pd['rulesets'][0]['result']); ?>
      </div>
    </div>
  </div>

  <?php
    if (isset($international) && $international) {
      print '<div id="pd-description-international" class="flex-component-description pd-block">';
    }
    else {
      print '<div id="pd-description" class="flex-component-description pd-block">';
    }
  ?>
    <div>
      <?php print $component_pd['rulesets'][0]['description'] ?>
    </div>
  </div>

  <div>
    <?php
      if (isset($international) && $international) {
          print '<p> If you\'re an international student, you may need to adjust your path
          through EDGE depending on your student visa and/or work permits. It\'s possible to complete
          EDGE with experiential learning courses and on-campus experiences that don\'t involve permits.
          For more information, contact the EDGE team at <a href="mailto:edge@uwaterloo.ca">edge@uwaterloo.ca</a>. </p>';
        }
      ?>
  </div>

  <div class="flex-component-title margin-top">
    <h2 class="edge-header">Capstone Workshop</h2>
  </div>

  <div class="flex-component-block">
    <div class="component_square">
      <div class="call-to-action-top-wrapper">
        <?php gen_href_start($component4['rulesets'][0]['result'], $component4['rulesets'][0]['url']); ?>
          <div class="call-to-action-wrapper">
            <div class="call-to-action-theme-uWaterloo">
              <div class="call-to-action-big-text"> <?php print $component4['rulesets'][0]['result'] ?> </div>
            </div>
          </div>
        <?php gen_href_end($component4['rulesets'][0]['result']); ?>
      </div>
    </div>
  </div>

  <div class="flex-component-description">
    <div>
      <?php print $component4['rulesets'][0]['description'] ?>
    </div>
  </div>

  <div class="flex-message margin-top">
    <p> View a
      <?php
        // International students.
        if (isset($international) && $international) {
          print l("PDF version of your EDGE path.", fillpdf_pdf_link($form_id = $fids[1], null, $webform = array('nid'=>$node->nid,'sid'=>$sid)) . '&token=' . $access_token);
        }
        // Non-international students.
        else {
          print l("PDF version of your EDGE path.", fillpdf_pdf_link($form_id = $fids[0], null, $webform = array('nid'=>$node->nid,'sid'=>$sid)) . '&token=' . $access_token);
        }
        ?>
    </p>
  </div>

  <div class="flex-back-button-wrapper">
    <div id ="back-button" class="edge-action-button-wrapper adjust-height">
      <div class="call-to-action-wrapper">
        <?php
          $url = url('/node/' . $node->nid);
          print '<a href="' . $url . '">';
        ?>
          <div class="call-to-action-wrapper adjust-height">
            <div class="edge-action-button-gray">
              <div class="call-to-action-big-text">
                <?php print t("Start over") ?>
              </div>
            </div>
          </div>
        </a>
      </div>
    </div>
  </div>

  <div class="flex-redo-button-wrapper">
    <div id="redo-button" class="edge-action-button-wrapper alignment adjust-height">
      <div class="call-to-action-wrapper">
        <a id="redo-button-href" href="">
          <div id="redo-hover-area" class="call-to-action-wrapper adjust-height">
            <div class="edge-action-button-gray">
              <div class="call-to-action-big-text">
                <?php print t("Generate new EDGE path") ?>
              </div>
            </div>
          </div>
        </a>
      </div>
    </div>
    <div class="text-box-hover-wrapper">
      <p class="text-box-hover">Generating a new path through EDGE may yield
        different results depending on your responses.
      </p>
    </div>
  </div>

</div>